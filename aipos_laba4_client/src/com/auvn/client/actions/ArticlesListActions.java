package com.auvn.client.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;

import javax.swing.JList;

import com.auvn.client.connection.ServiceRequests;
import com.auvn.client.gui.ArticleInfoFrame;
import com.auvn.client.gui.MainFrame;
import com.auvn.client.gui.panels.ArticleInfoPanel;
import com.auvn.client.gui.panels.MainPanel;

public class ArticlesListActions implements MouseListener {
	private ServiceRequests serviceRequests;
	private ArticleInfoPanel articleInfoPanel;
	private ArticleInfoFrame articleInfoFrame;
	private CategoriesTreeActions categoriesTreeActions;
	private MainFrame mainFrame;
	private MainPanel mainPanel;
	private JList<Object> articlesList;
	private Object[] tempObjs;
	private Vector<Object> articlesVector;
	private String retMsg;

	public ArticlesListActions(MainFrame mainFrame,
			ServiceRequests serviceRequests, ArticleInfoFrame articleInfoFrame) {
		this.serviceRequests = serviceRequests;
		this.articleInfoFrame = articleInfoFrame;
		this.articleInfoPanel = articleInfoFrame.getArticleInfoPanel();
		this.mainFrame = mainFrame;
		this.mainPanel = mainFrame.getMainPanel();
		this.articlesList = mainPanel.getArticlesList();
		articlesVector = new Vector<Object>();

	}

	public void showEditDialog() {
		if (articlesList.getSelectedIndex() != -1) {
			tempObjs = getArticleInfo();
			articleInfoFrame.setTitle("Edit");

			articleInfoPanel.getArticleNameTextField().setText(
					tempObjs[0].toString());
			articleInfoPanel.getArticleDataTextArea().setText(
					tempObjs[1].toString());

			articleInfoFrame.getArticleInfoPanel().getArticleDataTextArea()
					.setEditable(true);
			articleInfoFrame.getArticleInfoPanel().getArticleNameTextField()
					.setEditable(true);
			articleInfoFrame.setVisible(true);
		}
	}

	public void showAddDialog() {
		if (categoriesTreeActions.getCategoryPath() != null) {
			articleInfoFrame.setTitle("Add");
			articleInfoFrame.getArticleInfoPanel().getArticleDataTextArea()
					.setEditable(true);
			articleInfoFrame.getArticleInfoPanel().getArticleNameTextField()
					.setEditable(true);
			articleInfoFrame.setVisible(true);
		}
	}

	public String addArticle() {
		retMsg = serviceRequests
				.addArticle(categoriesTreeActions.getCategoryPath(),
						articleInfoPanel.getArticleNameTextField().getText(),
						articleInfoPanel.getArticleDataTextArea().getText());
		if (!retMsg.equals("Fail")) {
			articlesVector.addElement(articleInfoPanel
					.getArticleNameTextField().getText());
			articlesList.setListData(articlesVector);
		}
		return retMsg;
	}

	public void fillArticlesList() {
		System.out.println("Path :" + categoriesTreeActions.getCategoryPath());
		tempObjs = serviceRequests.getArticles(categoriesTreeActions
				.getCategoryPath());
		articlesVector.clear();
		for (Object article : tempObjs)
			articlesVector.addElement(article);
		articlesList.setListData(articlesVector);
	}

	public Object[] getArticleInfo() {
		return serviceRequests.getArticleInfo(categoriesTreeActions
				.getCategoryPath(), articlesList.getSelectedValue().toString());
	}

	public void setCategoriesTreeActions(
			CategoriesTreeActions categoriesTreeActions) {
		this.categoriesTreeActions = categoriesTreeActions;
	}

	public String removeArticle() {
		String tempSelectedStr = articlesList.getSelectedValue().toString();
		retMsg = serviceRequests.removeArticle(
				categoriesTreeActions.getCategoryPath(), tempSelectedStr);
		if (!retMsg.equals("Fail")) {
			articlesVector.remove(tempSelectedStr);
			articlesList.setListData(articlesVector);
			mainPanel.setButtonsStatesRemArticle();
		}

		return retMsg;
	}

	public String editArticle() {
		String tempSelectedStr = articlesList.getSelectedValue().toString(), newName;
		int index;
		retMsg = serviceRequests.editArticle(
				categoriesTreeActions.getCategoryPath(), tempSelectedStr,
				newName = articleInfoPanel.getArticleNameTextField().getText(),
				articleInfoPanel.getArticleDataTextArea().getText());
		if (!retMsg.equals("Fail")) {
			articlesVector.get(articlesList.getSelectedIndex());
			index = articlesVector.indexOf(tempSelectedStr);
			articlesVector.remove(index);
			articlesVector.insertElementAt(newName, index);
			articlesList.setListData(articlesVector);
			mainPanel.setButtonsStatesSelCategory();
		}
		return retMsg;
	}

	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount() == 2) {
			tempObjs = getArticleInfo();
			if (tempObjs.length != 0) {
				ArticleInfoFrame tempArticleInfoFrame = articleInfoFrame;
				if (tempArticleInfoFrame.isVisible()) {
					tempArticleInfoFrame = new ArticleInfoFrame(mainFrame);
				}

				tempArticleInfoFrame.getArticleInfoPanel()
						.getArticleNameTextField()
						.setText(tempObjs[0].toString());
				tempArticleInfoFrame.getArticleInfoPanel()
						.getArticleDataTextArea()
						.setText(tempObjs[1].toString());
				tempArticleInfoFrame.setTitle(tempObjs[0].toString());
				tempArticleInfoFrame.getArticleInfoPanel()
						.getArticleDataTextArea().setEditable(false);
				tempArticleInfoFrame.getArticleInfoPanel()
						.getArticleNameTextField().setEditable(false);
				tempArticleInfoFrame.setVisible(true);
			}
		} else {
			if (articlesList.getSelectedIndex() != -1) {
				mainPanel.setButtonsStatesSelArticle();
			}
		}
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
