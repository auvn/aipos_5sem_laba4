package com.auvn.client.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;

import com.auvn.client.gui.ArticleInfoFrame;
import com.auvn.client.gui.MainFrame;
import com.auvn.client.gui.panels.MainPanel;

public class ButtonsActions implements ActionListener {
	private MainFrame mainFrame;
	private MainPanel mainPanel;
	private JTree categoriesTree;
	private JButton pressedButton;
	private CategoriesTreeActions categoriesTreeActions;
	private ArticlesListActions articlesListActions;
	private String retCategoryName;
	private String retMsg;
	private ArticleInfoFrame articleInfoFrame;

	public ButtonsActions(MainFrame mainFrame,
			CategoriesTreeActions categoriesTreeActions,
			ArticlesListActions articlesListActions,
			ArticleInfoFrame articleInfoFrame) {
		this.mainFrame = mainFrame;
		this.categoriesTreeActions = categoriesTreeActions;
		this.articlesListActions = articlesListActions;
		this.mainPanel = mainFrame.getMainPanel();
		this.categoriesTree = this.mainPanel.getCategoriesTree();
		this.articleInfoFrame = articleInfoFrame;
	}

	public void actionPerformed(ActionEvent e) {
		pressedButton = (JButton) e.getSource();
		retMsg = "Fail";
		if (pressedButton == mainPanel.getAddCategory()) {
			retCategoryName = JOptionPane.showInputDialog(mainFrame,
					"Input new category name:");
			retMsg = "";
			if (retCategoryName != null && retCategoryName.length() != 0) {
				retMsg = categoriesTreeActions.addCategory(retCategoryName
						.replaceAll("[/]", "|"));
			}

		} else if (pressedButton == mainPanel.getRemoveCategory()) {
			retMsg = categoriesTreeActions
					.removeCategory(((DefaultMutableTreeNode) categoriesTree
							.getLastSelectedPathComponent()).getUserObject()
							.toString());
		} else if (pressedButton == mainPanel.getEditCategory()) {
			retCategoryName = JOptionPane.showInputDialog(mainFrame,
					"Input new category name:");
			retMsg = "";
			if (retCategoryName != null && retCategoryName.length() != 0)
				retMsg = categoriesTreeActions.editCategory(retCategoryName);
		} else if (pressedButton == mainPanel.getRemoveArticle()) {
			retMsg = articlesListActions.removeArticle();

		} else if (pressedButton == articleInfoFrame.getArticleInfoPanel()
				.getOkButton()) {
			retMsg = "Ok";
			if (articleInfoFrame.getTitle().equals("Add")) {
				retMsg = articlesListActions.addArticle();
			} else if (articleInfoFrame.getTitle().equals("Edit")) {
				retMsg = articlesListActions.editArticle();
			}
			articleInfoFrame.setVisible(false);
		}
		if (retMsg.equals("Ok"))
			categoriesTree.updateUI();
		else if (retMsg.equals("Fail"))
			JOptionPane.showMessageDialog(mainFrame,
					"Error in executing of action");
	}
}
