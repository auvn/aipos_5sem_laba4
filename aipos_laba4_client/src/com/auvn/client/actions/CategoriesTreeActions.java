package com.auvn.client.actions;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import com.auvn.client.connection.Procedures;
import com.auvn.client.connection.RpcServiceRequests;
import com.auvn.client.connection.ServiceRequests;
import com.auvn.client.gui.MainFrame;
import com.auvn.client.gui.panels.MainPanel;

public class CategoriesTreeActions implements MouseListener {
	private ServiceRequests serviceRequests;
	private ArticlesListActions articlesListActions;
	private DefaultTreeModel categoriesTreeModel;
	private DefaultMutableTreeNode rootCategoryNode, currentCategoryNode,
			tempNode;
	private String rootCategoryName;
	private JTree currentTree;
	private String retMsg;
	private MainPanel mainPanel;

	public CategoriesTreeActions(MainFrame mainFrame,
			ServiceRequests serviceRequests,
			ArticlesListActions articlesListActions) {
		this.serviceRequests = serviceRequests;
		this.articlesListActions = articlesListActions;
		this.mainPanel = mainFrame.getMainPanel();
		categoriesTreeModel = mainPanel.getCategoriesTreeModel();
		currentTree = mainPanel.getCategoriesTree();
	}

	public void initCategoriesList() {
		System.out.println(rootCategoryName = serviceRequests
				.getRootCategoryName());
		categoriesTreeModel
				.setRoot(rootCategoryNode = new DefaultMutableTreeNode(
						rootCategoryName));
		for (Object categoryObj : serviceRequests
				.getSubCategories(rootCategoryName)) {
			rootCategoryNode.add(new DefaultMutableTreeNode(categoryObj));
		}
	}

	private String getCategoryPath(Object[] treeNodes) {
		String categoryPath = "";
		for (int treeNodeIndex = 1; treeNodeIndex < treeNodes.length; treeNodeIndex++) {
			categoryPath += treeNodes[treeNodeIndex];
			if (treeNodeIndex + 1 != treeNodes.length)
				categoryPath += Procedures.PATH_SEPARATOR;
		}

		return categoryPath.equals("") ? "/" : categoryPath;
	}

	private boolean isChild(DefaultMutableTreeNode node, Object nodeName) {
		for (int i = 0; i < node.getChildCount(); i++)
			if (((DefaultMutableTreeNode) node.getChildAt(i)).getUserObject()
					.equals(nodeName))
				return true;
		return false;
	}

	private void addSubCategories(DefaultMutableTreeNode node,
			Object[] categories) {
		// node.removeAllChildren();
		for (Object category : categories) {
			if (category != null && !isChild(node, category)) {
				node.add(new DefaultMutableTreeNode(category));
			}
		}
	}

	public String addCategory(String categoryName) {
		System.out.println(categoryName);
		currentCategoryNode = (DefaultMutableTreeNode) currentTree
				.getLastSelectedPathComponent();
		retMsg = serviceRequests.addCategory(getCategoryPath(currentTree
				.getSelectionPath().getPath()), categoryName);
		if (retMsg.equals("Ok"))
			currentCategoryNode.add(new DefaultMutableTreeNode(categoryName));
		return retMsg;
	}

	public String removeCategory(String categoryName) {
		retMsg = "Fail";
		currentCategoryNode = (DefaultMutableTreeNode) currentTree
				.getLastSelectedPathComponent();
		tempNode = (DefaultMutableTreeNode) currentCategoryNode.getParent();
		if (tempNode != null) {
			retMsg = serviceRequests.removeCategory(
					getCategoryPath(tempNode.getPath()), categoryName);
			System.out.println(currentTree.getSelectionPath());
			if (retMsg.equals("Ok"))
				tempNode.remove(tempNode.getIndex(currentCategoryNode));
		}
		return retMsg;
	}

	public String editCategory(String categoryName) {
		currentCategoryNode = (DefaultMutableTreeNode) currentTree
				.getLastSelectedPathComponent();
		retMsg = serviceRequests.editCategory(getCategoryPath(currentTree
				.getSelectionPath().getPath()), categoryName);
		if (retMsg.equals("Ok"))
			currentCategoryNode.setUserObject(categoryName);
		return retMsg;
	}

	public void mouseClicked(MouseEvent e) {
		currentCategoryNode = (DefaultMutableTreeNode) currentTree
				.getLastSelectedPathComponent();
		if (currentCategoryNode != null) {

			if (e.getClickCount() != 2) {
				addSubCategories(currentCategoryNode,
						serviceRequests
								.getSubCategories(getCategoryPath(currentTree
										.getSelectionPath().getPath())));
				articlesListActions.fillArticlesList();
			}

			mainPanel.setButtonsStatesSelCategory();

			if (currentCategoryNode.getChildCount() != 0)
				currentTree.updateUI();
		}
	}

	public String getCategoryPath() {
		return this.getCategoryPath(currentTree.getSelectionPath().getPath());
	}

	public void mouseEntered(MouseEvent e) {

	}

	public void mouseExited(MouseEvent e) {

	}

	public void mousePressed(MouseEvent e) {

	}

	public void mouseReleased(MouseEvent e) {

	}
}
