package com.auvn.client.connection;

public interface Procedures {
	public static final String GET_ROOT_CATEGORY_NAME = "getRootCategoryName";
	public static final String GET_SUB_CATEGORIES = "getSubCategories";
	public static final String GET_ARTICLES = "getArticles";
	public static final String ADD_ARTICLE = "addArticle";
	public static final String REMOVE_ARTICLE = "removeArticle";
	public static final String EDIT_ARTICLE = "editArticle";
	public static final String GET_ARTICLE_INFO = "getArticleInfo";
	public static final String ADD_CATEGORY = "addCategory";
	public static final String REMOVE_CATEGORY = "removeCategory";
	public static final String EDIT_CATEGORY = "editCategory";
	public static final String PATH_SEPARATOR = "/";
}
