package com.auvn.client.connection;

public interface ServiceRequests {
	
	public String getRootCategoryName();

	public String[] getSubCategories(String categoryPath);

	public String[] getArticles(String categoryPath);

	public String addArticle(String articlePath, String newArticleName,
			String newArticleData);

	public String[] getArticleInfo(String articlePath, String articleName);

	public String removeArticle(String articlePath, String articleName);

	public String editArticle(String articlePath, String oldArticleName,
			String newArticleName, String newArticleData);

	public String addCategory(String categoryPath, String subCategoryName);

	public String removeCategory(String categoryPath, String categoryName);

	public String editCategory(String oldCategoryPath, String newCategoryName);
}
