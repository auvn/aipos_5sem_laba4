package com.auvn.client.connection;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Call;
import org.apache.axis.client.Service;


public class ServiceConnection {
	private Service service;
	private Call serviceCall;

	public ServiceConnection(String url) throws ServiceException {

		service = new Service();
		serviceCall = (Call) service.createCall();
		serviceCall.setTargetEndpointAddress(url);
	}

	public Object callProcedure(String name, Object[] args) throws AxisFault {
		return serviceCall.invoke(name, args);
	}

	public void connectToRpcService() {

	}

	public void connectToDataService() {

	}


}
