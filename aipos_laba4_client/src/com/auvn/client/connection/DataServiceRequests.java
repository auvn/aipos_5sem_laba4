package com.auvn.client.connection;

import java.rmi.RemoteException;

import org.apache.axis2.AxisFault;

import com.auvn.service.ServiceFileNotFoundExceptionException;
import com.auvn.service.ServiceIOExceptionException;
import com.auvn.service.ServiceStub;
import com.auvn.service.ServiceStub.AddArticle;
import com.auvn.service.ServiceStub.AddCategory;
import com.auvn.service.ServiceStub.EditArticle;
import com.auvn.service.ServiceStub.EditCategory;
import com.auvn.service.ServiceStub.GetArticleInfo;
import com.auvn.service.ServiceStub.GetArticles;
import com.auvn.service.ServiceStub.GetRootCategoryName;
import com.auvn.service.ServiceStub.GetSubCategories;
import com.auvn.service.ServiceStub.RemoveArticle;
import com.auvn.service.ServiceStub.RemoveCategory;

public class DataServiceRequests implements ServiceRequests {

	private ServiceStub serviceStub;
	private String[] ret = null;

	public DataServiceRequests() {
		try {
			this.serviceStub = new ServiceStub();
		} catch (AxisFault e) {
			e.printStackTrace();
		}
	}

	public String addArticle(String articlePath, String newArticleName,
			String newArticleData) {

		AddArticle addArticle = new AddArticle();
		addArticle.setArticlePath(articlePath);
		addArticle.setNewArticleData(newArticleData);
		addArticle.setNewArticleName(newArticleName);

		try {
			return serviceStub.addArticle(addArticle).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String getRootCategoryName() {
		try {
			return serviceStub.getRootCategoryName(new GetRootCategoryName())
					.get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return "";
	}

	public String[] getSubCategories(String categoryPath) {

		GetSubCategories getSubCategories = new GetSubCategories();
		getSubCategories.setCategoryName(categoryPath);
		try {
			ret = serviceStub.getSubCategories(getSubCategories).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		if (ret != null)
			return ret;
		return new String[] {};
	}

	public String[] getArticles(String categoryPath) {

		GetArticles getArticles = new GetArticles();
		getArticles.setCategoryName(categoryPath);

		try {

			ret = serviceStub.getArticles(getArticles).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		if (ret != null)
			return ret;
		return new String[] {};
	}

	public String[] getArticleInfo(String articlePath, String articleName) {
		GetArticleInfo getArticleInfo = new GetArticleInfo();
		getArticleInfo.setArticlePath(articlePath);
		getArticleInfo.setArticleName(articleName);

		try {
			ret = serviceStub.getArticleInfo(getArticleInfo).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		if (ret != null)
			return ret;
		return new String[] {};
	}

	public String removeArticle(String articlePath, String articleName) {
		RemoveArticle removeArticle = new RemoveArticle();
		removeArticle.setArticlePath(articlePath);
		removeArticle.setArticleName(articleName);

		try {
			return serviceStub.removeArticle(removeArticle).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}

		return "";
	}

	public String editArticle(String articlePath, String oldArticleName,
			String newArticleName, String newArticleData) {
		EditArticle editArticle = new EditArticle();
		editArticle.setArticlePath(articlePath);
		editArticle.setOldArticleName(oldArticleName);
		editArticle.setNewArticleName(newArticleName);
		editArticle.setNewArticleData(newArticleData);

		try {
			return serviceStub.editArticle(editArticle).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}

		return "";
	}

	public String addCategory(String categoryPath, String subCategoryName) {
		AddCategory addCategory = new AddCategory();

		addCategory.setCategoryPath(categoryPath);
		addCategory.setSubCategoryName(subCategoryName);

		try {
			return serviceStub.addCategory(addCategory).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}

		return "";
	}

	public String removeCategory(String categoryPath, String categoryName) {
		RemoveCategory removeCategory = new RemoveCategory();

		removeCategory.setCategoryPath(categoryPath);
		removeCategory.setCategoryName(categoryName);

		try {
			return serviceStub.removeCategory(removeCategory).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}

		return null;
	}

	public String editCategory(String oldCategoryPath, String newCategoryName) {
		EditCategory editCategory = new EditCategory();
		editCategory.setOldCategoryPath(oldCategoryPath);
		editCategory.setNewCategoryName(newCategoryName);

		try {
			return serviceStub.editCategory(editCategory).get_return();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceIOExceptionException e) {
			e.printStackTrace();
		} catch (ServiceFileNotFoundExceptionException e) {
			e.printStackTrace();
		}

		return "";
	}
}
