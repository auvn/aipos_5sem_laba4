package com.auvn.client;

import java.net.MalformedURLException;

import javax.xml.rpc.ServiceException;

import org.apache.axis.AxisFault;

import com.auvn.client.actions.ArticlesButtonsActions;
import com.auvn.client.actions.ArticlesListActions;
import com.auvn.client.actions.ButtonsActions;
import com.auvn.client.actions.CategoriesTreeActions;
import com.auvn.client.connection.DataServiceRequests;
import com.auvn.client.connection.ServiceConnection;
import com.auvn.client.connection.RpcServiceRequests;
import com.auvn.client.connection.ServiceRequests;
import com.auvn.client.gui.ArticleInfoFrame;
import com.auvn.client.gui.MainFrame;
import com.auvn.client.gui.panels.MainPanel;

public class Run {
	private static void init(MainFrame mainFrame)
			throws org.apache.axis2.AxisFault {
		ServiceConnection serviceConnection = null;
		try {
			serviceConnection = new ServiceConnection(
					"http://localhost:8080/axis/Service.jws");
		} catch (ServiceException e) {
			e.printStackTrace();

		}

		// ServiceRequests serviceRequests = new RpcServiceRequests(
		// serviceConnection);
		ServiceRequests serviceRequests = new DataServiceRequests();

		ArticleInfoFrame articleInfoFrame = new ArticleInfoFrame(mainFrame);

		ArticlesListActions articlesListActions = new ArticlesListActions(
				mainFrame, serviceRequests, articleInfoFrame);
		CategoriesTreeActions categoriesTreeActions = new CategoriesTreeActions(
				mainFrame, serviceRequests, articlesListActions);

		articlesListActions.setCategoriesTreeActions(categoriesTreeActions);

		MainPanel mainPanel = mainFrame.getMainPanel();
		mainPanel.setButtonsStatesRun();

		ButtonsActions buttonsActions = new ButtonsActions(mainFrame,
				categoriesTreeActions, articlesListActions, articleInfoFrame);
		ArticlesButtonsActions articlesButtonsActions = new ArticlesButtonsActions(
				mainFrame, articlesListActions);

		mainPanel.getAddCategory().addActionListener(buttonsActions);
		mainPanel.getRemoveCategory().addActionListener(buttonsActions);
		mainPanel.getEditCategory().addActionListener(buttonsActions);

		mainPanel.getAddArticle().addActionListener(articlesButtonsActions);
		mainPanel.getRemoveArticle().addActionListener(buttonsActions);
		mainPanel.getEditArticle().addActionListener(articlesButtonsActions);

		mainFrame.getMainPanel().getCategoriesTree()
				.addMouseListener(categoriesTreeActions);
		mainFrame.getMainPanel().getArticlesList()
				.addMouseListener(articlesListActions);

		articleInfoFrame.getArticleInfoPanel().getOkButton()
				.addActionListener(buttonsActions);

		categoriesTreeActions.initCategoriesList();

		mainFrame.setVisible(true);
	}

	public static void main(String[] args) throws ServiceException,
			MalformedURLException, AxisFault, org.apache.axis2.AxisFault {
		MainFrame mainFrame = new MainFrame();
		init(mainFrame);
	}
}
