package com.auvn.client.gui;

import javax.swing.JFrame;

import com.auvn.client.gui.panels.ArticleInfoPanel;

public class ArticleInfoFrame extends JFrame {
	private ArticleInfoPanel articleInfoPanel;
	private MainFrame mainFrame;

	public ArticleInfoFrame(MainFrame mainFrame) {
		this.mainFrame = mainFrame;

		if (articleInfoPanel == null) {
			articleInfoPanel = new ArticleInfoPanel();
			setContentPane(articleInfoPanel);
		}

		setSize(300, 300);
		setDefaultCloseOperation(HIDE_ON_CLOSE);

	}

	@Override
	public void setVisible(boolean b) {
		if (b) {
			setLocation(mainFrame.getLocation().x, mainFrame.getLocation().y);
		}
		super.setVisible(b);
	}

	public ArticleInfoPanel getArticleInfoPanel() {
		return articleInfoPanel;
	}

}
