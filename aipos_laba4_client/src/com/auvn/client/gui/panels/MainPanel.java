package com.auvn.client.gui.panels;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;

public class MainPanel extends JPanel {
	private JSplitPane manualContentSplitPane;
	private JList<Object> articlesList;
	private JTree categoriesTree;
	private DefaultTreeModel categoriesTreeModel;
	private JPanel categoriesPanel;
	private JPanel categoriesButtonsPanel;

	private JPanel articlesPanel;
	private JPanel articlesButtonsPanel;

	private JButton addCategory;
	private JButton editCategory;
	private JButton removeCategory;

	private JButton addArticle;
	private JButton editArticle;
	private JButton removeArticle;

	public MainPanel() {
		setLayout(new BorderLayout());

		categoriesPanel = new JPanel(new BorderLayout());
		categoriesButtonsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		articlesPanel = new JPanel(new BorderLayout());
		articlesButtonsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

		categoriesTreeModel = new DefaultTreeModel(new DefaultMutableTreeNode(
				"root", true));
		categoriesTree = new JTree(categoriesTreeModel);
		categoriesPanel.add(new JScrollPane(categoriesTree), BorderLayout.CENTER);
		categoriesTree.setSelectionPath(new TreePath("/"));
		
		addCategory = new JButton("Add");
		editCategory = new JButton("Edit");
		removeCategory = new JButton("Remove");
		categoriesButtonsPanel.add(addCategory);
		categoriesButtonsPanel.add(editCategory);
		categoriesButtonsPanel.add(removeCategory);
		categoriesPanel.add(categoriesButtonsPanel, BorderLayout.NORTH);

		addArticle = new JButton("Add");
		editArticle = new JButton("Edit");
		removeArticle = new JButton("Remove");
		articlesButtonsPanel.add(addArticle);
		articlesButtonsPanel.add(editArticle);
		articlesButtonsPanel.add(removeArticle);
		articlesPanel.add(articlesButtonsPanel, BorderLayout.NORTH);

		articlesList = new JList<Object>();

		articlesPanel.add(new JScrollPane(articlesList), BorderLayout.CENTER);

		manualContentSplitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
		manualContentSplitPane.setLeftComponent(new JScrollPane(categoriesPanel));
		manualContentSplitPane.setRightComponent(new JScrollPane(
				articlesPanel));
		manualContentSplitPane.setDividerLocation(240);

		add(manualContentSplitPane, BorderLayout.CENTER);
	}

	public void setButtonsStatesRun() {
		addArticle.setEnabled(false);
		addCategory.setEnabled(false);
		editArticle.setEnabled(false);
		editCategory.setEnabled(false);
		removeArticle.setEnabled(false);
		removeCategory.setEnabled(false);
	}

	public void setButtonsStatesSelCategory() {
		addArticle.setEnabled(true);
		addCategory.setEnabled(true);
		editArticle.setEnabled(false);
		editCategory.setEnabled(true);
		removeArticle.setEnabled(false);
		removeCategory.setEnabled(true);
	}

	public void setButtonsStatesSelArticle() {
		addArticle.setEnabled(true);
		addCategory.setEnabled(true);
		editArticle.setEnabled(true);
		editCategory.setEnabled(true);
		removeArticle.setEnabled(true);
		removeCategory.setEnabled(true);
	}

	public void setButtonsStatesRemArticle() {
		addArticle.setEnabled(true);
		addCategory.setEnabled(true);
		editArticle.setEnabled(false);
		editCategory.setEnabled(true);
		removeArticle.setEnabled(false);
		removeCategory.setEnabled(true);
	}

	public JList<Object> getArticlesList() {
		return articlesList;
	}

	public void setArticlesList(JList<Object> articlesList) {
		this.articlesList = articlesList;
	}

	public JTree getCategoriesTree() {
		return categoriesTree;
	}

	public void setCategoriesTree(JTree categoriesTree) {
		this.categoriesTree = categoriesTree;
	}

	public DefaultTreeModel getCategoriesTreeModel() {
		return categoriesTreeModel;
	}

	public JButton getAddCategory() {
		return addCategory;
	}

	public JButton getEditCategory() {
		return editCategory;
	}

	public JButton getRemoveCategory() {
		return removeCategory;
	}

	public JButton getAddArticle() {
		return addArticle;
	}

	public JButton getEditArticle() {
		return editArticle;
	}

	public JButton getRemoveArticle() {
		return removeArticle;
	}

}
