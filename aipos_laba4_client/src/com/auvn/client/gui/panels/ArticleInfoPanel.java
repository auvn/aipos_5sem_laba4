package com.auvn.client.gui.panels;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class ArticleInfoPanel extends JPanel {
	private JTextArea articleDataTextArea;
	private JTextField articleNameTextField;
	private JPanel buttonsPanel;

	private JButton okButton;

	public ArticleInfoPanel() {
		setLayout(new BorderLayout());

		buttonsPanel = new JPanel(new FlowLayout());

		okButton = new JButton("Ok");

		buttonsPanel.add(okButton);

		articleNameTextField = new JTextField("New Article");

		articleDataTextArea = new JTextArea("Content");
		articleDataTextArea.setLineWrap(true);
		articleDataTextArea.setWrapStyleWord(true);

		add(articleNameTextField, BorderLayout.NORTH);
		add(new JScrollPane(articleDataTextArea), BorderLayout.CENTER);
		add(buttonsPanel, BorderLayout.SOUTH);
	}

	public JTextArea getArticleDataTextArea() {
		return articleDataTextArea;
	}

	public void setArticleDataTextArea(JTextArea articleDataTextArea) {
		this.articleDataTextArea = articleDataTextArea;
	}

	public JTextField getArticleNameTextField() {
		return articleNameTextField;
	}

	public void setArticleNameTextField(JTextField articleNameTextField) {
		this.articleNameTextField = articleNameTextField;
	}

	public JButton getOkButton() {
		return okButton;
	}

	public void setOkButton(JButton okButton) {
		this.okButton = okButton;
	}

}
