
/**
 * ServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.2  Built on : Apr 17, 2012 (05:33:49 IST)
 */

    package com.auvn.service;

    /**
     *  ServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for getArticles method
            * override this method for handling normal response from getArticles operation
            */
           public void receiveResultgetArticles(
                    com.auvn.service.ServiceStub.GetArticlesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getArticles operation
           */
            public void receiveErrorgetArticles(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getArticleInfo method
            * override this method for handling normal response from getArticleInfo operation
            */
           public void receiveResultgetArticleInfo(
                    com.auvn.service.ServiceStub.GetArticleInfoResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getArticleInfo operation
           */
            public void receiveErrorgetArticleInfo(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRootCategoryName method
            * override this method for handling normal response from getRootCategoryName operation
            */
           public void receiveResultgetRootCategoryName(
                    com.auvn.service.ServiceStub.GetRootCategoryNameResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRootCategoryName operation
           */
            public void receiveErrorgetRootCategoryName(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeArticle method
            * override this method for handling normal response from removeArticle operation
            */
           public void receiveResultremoveArticle(
                    com.auvn.service.ServiceStub.RemoveArticleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeArticle operation
           */
            public void receiveErrorremoveArticle(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getSubCategories method
            * override this method for handling normal response from getSubCategories operation
            */
           public void receiveResultgetSubCategories(
                    com.auvn.service.ServiceStub.GetSubCategoriesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getSubCategories operation
           */
            public void receiveErrorgetSubCategories(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for removeCategory method
            * override this method for handling normal response from removeCategory operation
            */
           public void receiveResultremoveCategory(
                    com.auvn.service.ServiceStub.RemoveCategoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from removeCategory operation
           */
            public void receiveErrorremoveCategory(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editCategory method
            * override this method for handling normal response from editCategory operation
            */
           public void receiveResulteditCategory(
                    com.auvn.service.ServiceStub.EditCategoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editCategory operation
           */
            public void receiveErroreditCategory(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for editArticle method
            * override this method for handling normal response from editArticle operation
            */
           public void receiveResulteditArticle(
                    com.auvn.service.ServiceStub.EditArticleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from editArticle operation
           */
            public void receiveErroreditArticle(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addArticle method
            * override this method for handling normal response from addArticle operation
            */
           public void receiveResultaddArticle(
                    com.auvn.service.ServiceStub.AddArticleResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addArticle operation
           */
            public void receiveErroraddArticle(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getRootCategory method
            * override this method for handling normal response from getRootCategory operation
            */
           public void receiveResultgetRootCategory(
                    com.auvn.service.ServiceStub.GetRootCategoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getRootCategory operation
           */
            public void receiveErrorgetRootCategory(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for addCategory method
            * override this method for handling normal response from addCategory operation
            */
           public void receiveResultaddCategory(
                    com.auvn.service.ServiceStub.AddCategoryResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from addCategory operation
           */
            public void receiveErroraddCategory(java.lang.Exception e) {
            }
                


    }
    